// import GaodeMap from './amap/';
import GaodeMapV1 from './amap/';
import GaodeMap from './amap2/';
import Map from './map/';
import Mapbox from './mapbox/';

export { GaodeMap, GaodeMapV1, Mapbox, Map };
